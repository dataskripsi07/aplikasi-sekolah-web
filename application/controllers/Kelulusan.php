<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Kelulusan extends CI_Controller {

	public function index()
	{
		$data = array(
            'judul_page' => "Data Siswa Terima",
            'konten' => 'lulus/view',
        );
        $this->load->view('v_index', $data);
	}

	public function delete($id_user)
	{
		$this->db->where('id_user', $id_user);
		$this->db->delete('hasil_seleksi');

		$this->session->set_flashdata('pesan', alert_biasa('Seleksi berhasil dihapus!','success'));
		redirect('kelulusan','refresh');
	}

}

/* End of file Kelulusan.php */
/* Location: ./application/controllers/Kelulusan.php */