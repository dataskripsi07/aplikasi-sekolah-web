<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {


     public function index()
     {
          if ($this->session->userdata('level') == '') {
               redirect('login');
          }
          $data = array(
               'judul_page' => "Dashboard",
               'konten' => 'home/view',
          );
          $this->load->view('v_index', $data);
     }

     public function users()
     {
          if ($this->session->userdata('level') == '') {
               redirect('login');
          }
          $data = array(
               'judul_page' => "Data User",
               'konten' => 'users/view',
          );
          $this->load->view('v_index', $data);
     }

     public function reset_password($id_user)
     {
          $this->db->where('id_user', $id_user);
          $this->db->update('users', ['password' => '123456']);
          $this->session->set_flashdata('pesan', alert_biasa('berhasil reset password menjadi 123456','success'));
          redirect("app/users");
     }

     public function reset_data($table)
     {
          $this->db->truncate($table);
          $this->session->set_flashdata('pesan', alert_biasa('Berhasil di reset','success'));
          redirect($table);
     }

     public function dev()
     {
          $this->session->set_flashdata('pesan', alert_biasa('Sedang dalam pengembangan','info'));
          redirect("app");
     }

     public function cetak_view()
     {
          if ($this->session->userdata('level') == '') {
               redirect('login');
          }
          $data = array(
               'judul_page' => "Cetak Tindakan",
               'konten' => 'cetak/view',
          );
          $this->load->view('v_index', $data);
     }


}

/* End of file App.php */
