<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function index()
	{
		echo "API";
	}


	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$cek = $this->db->query("SELECT users.*, pendaftaran.nama FROM users inner join pendaftaran ON users.id_pendaftaran=pendaftaran.id_pendaftaran WHERE users.username='$username' and users.password='$password' ");
		if ($cek->num_rows() > 0) {
			$user = $cek->row();
			$token = base64_encode($username.":".$password);
			$message = [
				'error' => "true",
				'message' => "success login",
				'data' => [
					[
						'id_user' => $user->id_user,
			            'username' => $user->username,
			            'id_pendaftaran' => $user->id_pendaftaran,
			            'nama_lengkap' => $user->nama,
			            'token' => $token
					]
				]
	            
	        ];

	        echo json_encode($message);
			
		} else {
		   	$message = [
				'error' => "false",
				'message' => "gagal login",
				'data' => []
	        ];
	        echo json_encode($message);
		}
	}

	public function daftar()
	{
		$nama_lengkap = str_replace('"',"",$this->input->post('nama_lengkap'));
		$tanggal_lahir = str_replace('"',"",$this->input->post('tanggal_lahir'));
		$jenis_kelamin = str_replace('"',"",$this->input->post('jenis_kelamin'));
		$agama = str_replace('"',"",$this->input->post('agama'));
		$no_telp_ayah = str_replace('"',"",$this->input->post('no_telp_ayah'));
		$no_telp_ibu = str_replace('"',"",$this->input->post('no_telp_ibu'));
		$nama_ayah = str_replace('"',"",$this->input->post('nama_ayah'));
		$nama_ibu = str_replace('"',"",$this->input->post('nama_ibu'));
		$username = str_replace('"',"",$this->input->post('username'));
		$password = str_replace('"',"",$this->input->post('password'));

		$foto = ($_FILES['foto_3x4']['name'] == '') ? NULL : upload_gambar_biasa('foto', 'image/foto/', 'jpeg|png|jpg|gif', 10000, 'foto_3x4');
		$foto_kk = ($_FILES['foto_kk']['name'] == '') ? NULL : upload_gambar_biasa('kk', 'image/kk/', 'jpeg|png|jpg|gif', 10000, 'foto_kk');
		$foto_akte = ($_FILES['foto_akte']['name'] == '') ? NULL : upload_gambar_biasa('akte', 'image/akte/', 'jpeg|png|jpg|gif', 10000, 'foto_akte');
		
		$data = array(
			'nama' => $nama_lengkap,
			'tanggal_lahir' => $tanggal_lahir,
			'jenis_kelamin' => $jenis_kelamin,
			'agama' => $agama,
			'no_telp_ayah' => $no_telp_ayah,
			'no_telp_ibu' => $no_telp_ibu,
			'nama_ayah' => $nama_ayah,
			'nama_ibu' => $nama_ibu,
			'foto' => $foto,
			'foto_kk' => $foto_kk,
			'foto_akte' => $foto_akte,
			'id_periode' => get_data('periode','aktif', 'y','id_periode'),
			'created_at' => get_waktu()
		);

		$this->db->insert('pendaftaran', $data);
		$id = $this->db->insert_id();
		$this->db->insert('users', array('id_pendaftaran' => $this->db->insert_id(), 'username' => $username, 'password' => $password));

		$message = [
			'error' => "true",
			'message' => "Pendaftaran siswa baru berhasil",
			'data' => []
        ];
        echo json_encode($message);
	}

	public function get_pendaftaran()
	{
		$id_pendaftaran = $this->input->post('id_pendaftaran');
		$id_user = $this->input->post('id_user');
		$this->db->where('id_pendaftaran', $id_pendaftaran);
		$pendaftaran = $this->db->get('pendaftaran')->row();

		$this->db->where('id_user', $id_user);
        $check_admin = $this->db->get('hasil_seleksi_administrasi');
        if ($check_admin->num_rows() > 0) {
            $lulus = 'Lulus Administrasi';
			$tgl_tes = $check_admin->row()->tanggal_tes;
			$admin_user = get_data('admin','id_admin', $check_admin->row()->id_admin, 'nama');
        } else {
            $lulus = "Proses";
			$tgl_tes = 'N/A';
			$admin_user = 'N/A';
        }
		

		$this->db->where('id_user', $id_user);
        $check = $this->db->get('hasil_seleksi');
        if ($check->num_rows() > 0) {
            $lulus = 'Lulus';
			$admin_user = get_data('admin','id_admin', $check->row()->id_admin, 'nama');
        }

		$message = [
				'error' => "true",
				'message' => "success",
				'data' => [
					[
						'no_pendaftaran' => "PSB".$pendaftaran->id_pendaftaran,
						'nama' => $pendaftaran->nama,
						'nama_ayah' => $pendaftaran->nama_ayah,
						'nama_ibu' => $pendaftaran->nama_ibu,
						'tgl_daftar' => $pendaftaran->created_at,
						'lulus' => $lulus,
						'tgl_tes' => $tgl_tes,
						'admin_user' => $admin_user,
						'periode' => get_data('periode','id_periode',$pendaftaran->id_periode,'periode'),
					]
				]
	            
	        ];
		

	    echo json_encode($message);
		
	}

}

/* End of file Api.php */
/* Location: ./application/controllers/Api.php */