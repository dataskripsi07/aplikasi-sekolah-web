<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

     public function index()
     {
          $this->load->view('login');
          
     }

     public function auth()
     {
          $username = $this->input->post('username');
          $password = $this->input->post('password');
          $vusername = false;
          $vpassword = false;
          if ($username == 'admin') {
               $level = "admin";
               $cek = $this->db->query("SELECT * FROM admin WHERE username='$username' and password='$password' ");
          } else {
               $level = "siswa";

               $cek = $this->db->query("SELECT * FROM users WHERE username='$username' and password='$password' ");

               $cek_username = $this->db->get_where('users', array('username' => $username));
               if ($cek_username->num_rows() == 0) {
                    $vusername = true;
               } 

               $cek_password = $this->db->get_where('users', array('password' => $password));
               if ($cek_password->num_rows() == 0) {
                    $vpassword = true;
               } 

               if ( $vusername and $vpassword ) {
                    $this->session->set_flashdata('pesan', alert_biasa('Username dan passwor belum terdaftar!','warning'));
                    redirect("login");
                    exit;
               } elseif ( $vusername ) {
                    $this->session->set_flashdata('pesan', alert_biasa('Username salah !','warning'));
                    redirect("login");
                    exit;
               } elseif ( $vpassword ){
                    $this->session->set_flashdata('pesan', alert_biasa('password salah !','warning'));
                    redirect("login");
                    exit;
               }


          }
          
          
          if ($cek->num_rows() > 0) {
               $user = $cek->row();
               if ($level == 'admin') {
                    $sess_data['id_user'] = $user->id_admin;
               } else {
                    $sess_data['id_user'] = $user->id_pendaftaran;
               }
               
               $sess_data['username'] = $user->username;
               $sess_data['level'] = $level;
               $this->session->set_userdata($sess_data);
               redirect('app','refresh');
               
          } else {
               $this->session->set_flashdata('pesan', alert_biasa('Username dan passwor belum terdaftar!','warning'));
               redirect("login");
          }
          
     }

     public function logout()
     {
          $this->session->unset_userdata('id_user');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('nama');
          $this->session->unset_userdata('foto');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('login','refresh');
     }

}

/* End of file Login.php */
