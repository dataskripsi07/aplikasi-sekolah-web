<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran extends CI_Controller {

	public function index()
	{
		$this->load->view('pendaftaran');
	}

	public function simpan_pendaftaran()
	{
		$username = $this->input->post('username');
		$cek_username = $this->db->get_where('users', array('username' => $username));
		if ($cek_username->num_rows() > 0) {
			$this->session->set_flashdata('pesan', alert_biasa('username sudah ada, silahkan ulangi','warning'));
               // redirect("login?username=".$username);
			redirect('pendaftaran','refresh');
			exit;
		}
		$password = $this->input->post('password');
		$allowed_mime_type_arr = array('image/jpeg');
        if ( !in_array(get_mime_by_extension($_FILES['foto']['name']), $allowed_mime_type_arr) ) {
        	$this->session->set_flashdata('pesan', alert_biasa('file foto harus format jpeg','warning'));
			redirect('pendaftaran','refresh');
			exit;
        } elseif ( !in_array(get_mime_by_extension($_FILES['kk']['name']), $allowed_mime_type_arr) ) {
        	$this->session->set_flashdata('pesan', alert_biasa('file kk harus format jpeg','warning'));
			redirect('pendaftaran','refresh');
			exit;
        } elseif ( !in_array(get_mime_by_extension($_FILES['akte']['name']), $allowed_mime_type_arr) ) {
        	$this->session->set_flashdata('pesan', alert_biasa('file akte harus format jpeg','warning'));
			redirect('pendaftaran','refresh');
			exit;
        }

		$foto = ($_FILES['foto']['name'] == '') ? NULL : upload_gambar_biasa('foto', 'image/foto/', 'jpeg|png|jpg|gif', 10000, 'foto');
		$foto_kk = ($_FILES['kk']['name'] == '') ? NULL : upload_gambar_biasa('kk', 'image/kk/', 'jpeg|png|jpg|gif', 10000, 'kk');
		$foto_akte = ($_FILES['akte']['name'] == '') ? NULL : upload_gambar_biasa('akte', 'image/akte/', 'jpeg|png|jpg|gif', 10000, 'akte');
		$_POST['id_periode'] = get_data('periode','aktif', 'y','id_periode');
		$_POST['foto'] = $foto;
		$_POST['foto_kk'] = $foto_kk;
		$_POST['foto_akte'] = $foto_akte;
		$_POST['created_at'] = get_waktu();
		unset($_POST['username']);
		unset($_POST['password']);
		$this->db->insert('pendaftaran', $_POST);
		$id = $this->db->insert_id();
		$this->db->insert('users', array('id_pendaftaran' => $this->db->insert_id(), 'username' => $username, 'password' => $password));
		$this->session->set_flashdata('pesan', alert_biasa('Pendaftaran berhasil, silahkan login!','success'));
               // redirect("login?username=".$username);
		redirect('login','refresh');
	}

	public function view()
	{
		$data = array(
            'judul_page' => "Data Pendaftaran",
            'konten' => 'pendaftaran/view',
        );
        $this->load->view('v_index', $data);
	}

	public function lihat($id_pendaftaran)
	{
		$data = array(
            'judul_page' => "Detail Pendaftaran",
            'konten' => 'pendaftaran/lihat',
        );
        $this->load->view('v_index', $data);
	}

	public function approve($id_pendaftaran)
	{
		$id_user = $this->db->get_where('users', ['id_pendaftaran'=>$id_pendaftaran])->row()->id_user;
		$this->db->insert('hasil_seleksi', array(
			'id_user' => $id_user,
			'created_at' => get_waktu(),
			'id_admin' => $this->session->userdata('id_user')
		));
		$this->session->set_flashdata('pesan', alert_biasa('Siswa berhasil di set lulus seleksi!','success'));
               // redirect("login?username=".$username);
		redirect('pendaftaran/view','refresh');
		
	}

	public function approve_administrasi($id_pendaftaran)
	{
		$id_user = $this->db->get_where('users', ['id_pendaftaran'=>$id_pendaftaran])->row()->id_user;
		$tgl1    = date('Y-m-d');
		$tgl2    = date('Y-m-d', strtotime('+2 days', strtotime($tgl1)));
		$this->db->insert('hasil_seleksi_administrasi', array(
			'id_user' => $id_user,
			'tanggal_tes' => $tgl2.' 08:00:00',
			'created_at' => get_waktu(),
			'id_admin' => $this->session->userdata('id_user')
		));
		$this->session->set_flashdata('pesan', alert_biasa('Siswa berhasil di set lulus seleksi administrasi!','success'));
               // redirect("login?username=".$username);
		redirect('pendaftaran/view','refresh');
		
	}

	public function delete($id_pendaftaran)
	{
		$this->db->where('id_pendaftaran', $id_pendaftaran);
		$this->db->delete('pendaftaran');

		$this->db->where('id_pendaftaran', $id_pendaftaran);
		$this->db->delete('users');

		$this->session->set_flashdata('pesan', alert_biasa('Pendaftaran berhasil dihapus!','success'));
		redirect('pendaftaran/view','refresh');
	}

}

/* End of file Pendaftaran.php */
/* Location: ./application/controllers/Pendaftaran.php */