<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Kelulusan_administrasi extends CI_Controller {

	public function index()
	{
		$data = array(
            'judul_page' => "Data Siswa Lulus Administrasi",
            'konten' => 'lulus_administrasi/view',
        );
        $this->load->view('v_index', $data);
	}

	public function delete($id_user)
	{
		$this->db->where('id_user', $id_user);
		$this->db->delete('hasil_seleksi_administrasi');

		$this->session->set_flashdata('pesan', alert_biasa('Seleksi berhasil dihapus!','success'));
		redirect('kelulusan','refresh');
	}

}

/* End of file Kelulusan.php */
/* Location: ./application/controllers/Kelulusan.php */