<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	 <div class="card border-top border-0 border-4 border-info">
     <div class="card-body">
          <div class="border p-4 rounded">
               <div class="card-title d-flex align-items-center">
                    <div><i class="bx bx-edit-alt me-1 font-22 text-info"></i>
                    </div>
                    <h5 class="mb-0 text-info"><?php echo $judul_form ?></h5>
               </div>
               <hr />
               
	 <div class="row mb-3">
                    <label for="periode" class="col-sm-3
                         col-form-label">Periode
                         <?php echo form_error('periode') ?></label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" name="periode"
                              id="periode" placeholder="Periode"
                              value="<?php echo $periode; ?>" />
                    </div>
               </div>
	 <div class="row mb-3">
                    <label for="aktif" class="col-sm-3
                         col-form-label">Aktif
                         <?php echo form_error('aktif') ?></label>
                    <div class="col-sm-9">
                         <!-- <input type="text" class="form-control" name="aktif"
                              id="aktif" placeholder="Aktif"
                              value="<?php echo $aktif; ?>" /> -->
                         <select name="aktif" class="form-control">
                              <option value="<?php echo $aktif ?>"><?php echo $aktif ?></option>
                              <option value="y">Y</option>
                              <option value="t">T</option>
                         </select>
                    </div>
               </div>
	 <input type="hidden" name="id_periode" value="<?php echo $id_periode; ?>" /> 
	 <button type="submit" class="btn btn-primary"><i class="bx bx-save"></i>
                    <?php echo $button ?></button> 
	 <a href="<?php echo site_url('periode') ?>" class="btn btn-outline-info"><i
                         class="bx bx-exit"></i> Cancel</a>
	
               </div>
               </div>
               </div>
               </form>
               