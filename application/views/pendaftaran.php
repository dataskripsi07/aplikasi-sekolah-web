<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Pendaftaran Sekolah Dasar</title>
  <base href="<?php echo base_url() ?>">
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="front/assets/img/favicon.png" rel="icon">
  <link href="front/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="front/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="front/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="front/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="front/assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="front/assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="front/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="front/assets/vendor/simple-datatables/style.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="front/assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.4.1
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <main>
    <div class="container">

      <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-8 col-md-6 d-flex flex-column align-items-center justify-content-center">

              <div class="d-flex justify-content-center py-4">
                <a href="" class="logo align-items-center w-auto">
                  <center>
                    <img src="image/sd.png" alt="">
                  </center>
                  <span class="d-none d-lg-block" style="margin-top: 20px;">SDN 011/XI DESA GEDANG SUNGAI PENUH</span>
                </a>
              </div><!-- End Logo -->

              <div class="card mb-3">

                <div class="card-body">

                  <div class="pt-4 pb-2">
                    <h5 class="card-title text-center pb-0 fs-4">Pendaftaran Siswa Baru</h5>
                    <p class="text-center small"><?php echo get_data('periode','aktif', 'y','periode') ?></p>

                    <div class="col-12" style="margin-top: 50px">
                      <p class="small mb-0">Sudah memiliki akun, silahkan login disini? <a href="login">LOGIN</a></p>
                    </div>
                    <hr>
                  </div>

                  <form action="Pendaftaran/simpan_pendaftaran" method="POST" enctype="multipart/form-data" class="row g-3 needs-validation" novalidate>
                    
                    <div class="col-12">
                      <label class="form-label">Username</label>
                      <input type="text" name="username" class="form-control" required>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>

                    <div class="col-12">
                      <label class="form-label">Password</label>
                      <input type="text" name="password" class="form-control" required>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>

                    <div class="col-12">
                      <label class="form-label">Nama Lengkap</label>
                      <input type="text" name="nama" class="form-control" required>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>

                    <div class="col-12">
                      <label class="form-label">Asal TK</label>
                      <input type="text" name="asal_tk" class="form-control" placeholder="jika tidak ada harap isi dengan N/A">
                    </div>

                    <div class="col-12">
                      <label class="form-label">Alamat</label>
                      <textarea class="form-control" name="alamat"></textarea>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>

                    <div class="col-12">
                      <label class="form-label">Tempat Lahir</label>
                      <input type="text" name="tempat_lahir" class="form-control" required>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>

                    <div class="col-12">
                      <label class="form-label">Tanggal Lahir</label>
                      <input type="date" name="tanggal_lahir" class="form-control" required>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>

                    <div class="col-12">
                      <label class="form-label">Jenis Kelamin</label>
                      <select class="form-control" name="jenis_kelamin" required>
                        <option value="">--Pilih Jenis Kelamin--</option>
                        <option value="Laki-laki">Laki-laki</option>
                        <option value="Perempuan">Perempuan</option>
                      </select>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>

                    <div class="col-12">
                      <label class="form-label">Agama</label>
                      <select class="form-control" name="agama" required>
                        <option value="">--Pilih Agama--</option>
                        <option value="Islam">Islam</option>
                        <option value="Kristen">Kristen</option>
                        <option value="Budha">Budha</option>
                        <option value="Hindu">Hindu</option>
                        <option value="Konghucu">Konghucu</option>
                      </select>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>

                    <div class="col-12">
                      <label class="form-label">Nama Ayah</label>
                      <input type="text" name="nama_ayah" class="form-control" required>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>
                    <div class="col-12">
                      <label class="form-label">Nama Ibu</label>
                      <input type="text" name="nama_ibu" class="form-control" required>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>


                    <div class="col-12">
                      <label class="form-label">No Telp Ayah</label>
                      <input type="text" name="no_telp_ayah" class="form-control" required>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>
                    <div class="col-12">
                      <label class="form-label">No Telp Ibu</label>
                      <input type="text" name="no_telp_ibu" class="form-control" required>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>

                    <div class="col-12">
                      <label class="form-label">Upload Pas Photo 3x4</label>
                      <input type="file" name="foto" class="form-control" required>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>

                    <div class="col-12">
                      <label class="form-label">Upload KK</label>
                      <input type="file" name="kk" class="form-control" required>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>

                    <div class="col-12">
                      <label class="form-label">Upload Akte Kelahiran</label>
                      <input type="file" name="akte" class="form-control" required>
                      <div class="invalid-feedback">Isian tidak boleh kosong!</div>
                    </div>

                    
                    <div class="col-12">
                      <button class="btn btn-primary w-100" type="submit">Daftar Sekarang</button>
                    </div>
                    
                  </form>

                </div>
              </div>

              <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
                Copyright @<a href="#"> SDN 011/XI DESA GEDANG SUNGAI PENUH</a>
              </div>

            </div>
          </div>
        </div>

      </section>

    </div>
  </main><!-- End #main -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="front/assets/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="front/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="front/assets/vendor/chart.js/chart.min.js"></script>
  <script src="front/assets/vendor/echarts/echarts.min.js"></script>
  <script src="front/assets/vendor/quill/quill.min.js"></script>
  <script src="front/assets/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="front/assets/vendor/tinymce/tinymce.min.js"></script>
  <script src="front/assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="front/assets/js/main.js"></script>

  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript"><?php echo $this->session->userdata('pesan') ?></script>

</body>

</html>