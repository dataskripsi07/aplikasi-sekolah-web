
      


        <div class="card">
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col">
                        
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="exampleDataTable" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Pendaftaran</th>
                                <th>Nama Lengkap</th>
                                <th>Tanggal Daftar</th>
                                <th>Status</th>
                                <th>Tanggal Lulus</th>
                                <th>Admin User</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        <tbody><?php
                        $no = 1;
                        $lulus = "";
                        $where = "";
                        if ($this->session->userdata('level') == 'siswa') {
                            $id_pendaftaran =  $this->session->userdata('id_user');
                            $where = "WHERE a.id_pendaftaran='$id_pendaftaran' ";
                        }
                        $sql = "SELECT a.*, b.id_user, c.id_admin,c.created_at as tgl_lulus FROM pendaftaran as a INNER JOIN users as b ON a.id_pendaftaran=b.id_pendaftaran INNER JOIN hasil_seleksi as c ON b.id_user=c.id_user $where";
                        $pendafataran_data = $this->db->query($sql);
                        foreach ($pendafataran_data->result() as $pendaftaran)
                        {
                            ?>
                            <tr>
            <td width="80px"><?php echo $no ?></td>
            <td><?php echo "PSB".$pendaftaran->id_pendaftaran ?></td>
            <td><?php echo $pendaftaran->nama ?></td>
            <td><?php echo $pendaftaran->created_at ?></td>
            <td>
                <?php 
                $get_lulus = "";
                $this->db->where('id_user', $pendaftaran->id_user);
                $check = $this->db->get('hasil_seleksi');
                if ($check->num_rows() > 0) {
                    $lulus = 'ya';
                    $get_lulus = '?lulus=y&admin_user='.get_data('admin','id_admin', $pendaftaran->id_admin, 'nama');
                    echo "<b style='color: green'>Lulus</b>";
                } else {
                    $lulus = "tidak";
                    echo "<b>Proses</b>";
                }

                 ?>         
            </td>
            <td><?php echo $pendaftaran->tgl_lulus ?></td>
            <td><?php echo get_data('admin','id_admin', $pendaftaran->id_admin, 'nama') ?></td>
            <td style="text-align:center" width="200px">

                        <a href="Pendaftaran/lihat/<?php echo $pendaftaran->id_pendaftaran.$get_lulus ?>" title="Lihat Data" class="btn btn-sm btn-primary">Lihat
                        </a>
                        <?php if ($this->session->userdata('level') == 'admin'): ?>
                            <a href="kelulusan/delete/<?php echo $pendaftaran->id_user ?>" title="Hapus Data" onclick="javasciprt: return confirm('Yakin akan hapus data ini ?')" class="btn btn-sm btn-danger"><i class="bx bx-trash-alt me-0"></i>
                            </a>
                        <?php endif ?>
                        
            
            </td>
        </tr>
                            <?php
                            $no++;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    